from src.grid_stuffs.data import GRID
from src.grid_stuffs.cli import main

"""This is sample if NOT all nodes are all satisfied.
Expected:
    You should see print outs:
    [WARNING ] [main] There are still [x] unsatisfied nodes.
    [WARNING ] [main] Increasing the 'iteration' might solve this.
    [INFO    ] [main] satisfied nodes: [x]
"""

threshold = 4
iteration = 2000

main(threshold, iteration, GRID)

