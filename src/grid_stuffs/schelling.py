import numpy as np
import random


class GridNoEmptyNodeError(Exception):
    pass


class GridInvalidNodeTypesCount(Exception):
    pass


class Schelling:
    """A class that implements the Schelling Model.
    https://en.wikipedia.org/wiki/Schelling%27s_model_of_segregation
    Arguments:
        :param grid: A 2D array list
    """
    def __init__(self, grid: list):
        self.grid = np.array(grid)  # convert the array to numpy object
        self.nodes_types = np.unique(self.grid)
        self.x_max, self.y_max = self.grid.shape
        self.node_types = np.unique(self.grid)
        self.all_nodes = self.empty_nodes = []

    def get_node_list(self):
        # reset node list every time
        self.all_nodes = self.empty_nodes = []
        # get all grid nodes
        for i in range(self.x_max):
            for j in range(self.y_max):
                self.all_nodes.append((i, j))
        # get all nodes with empty (' ') values
        pos = np.where(self.grid == ' ')
        self.empty_nodes = list(zip(pos[0], pos[1]))

    def validate_node_types(self):
        """Method that checks if the inputted grid array is valid for this class.
        """
        if ' ' not in self.nodes_types:
            raise GridNoEmptyNodeError("There should be a blank (' ') node type.")

        if len(self.nodes_types) != 3:
            raise GridInvalidNodeTypesCount('There should be only two (3) different node types')

    def get_neighbor_nodes(self, node: tuple):
        """Method that returns the indices neighbor nodes of an inputted node
        Argument:
            :param node: determiner if a node is satisfied or not
                e.g. node (0, 0)
        returns [(1,0),  (0,1),  (1,1)] -> [bottom, right, bottom right]
        See in-line comments for more details
        """
        x, y = node[0], node[1]
        if x == 0 and y == 0:       # top left corner
            return [(1, 0),         # bottom
                    (0, 1),         # right
                    (1, 1)          # bottom right
                    ]
        elif x == (self.x_max - 1) and y == (self.y_max - 1):   # bot right corner
            return [(x, y - 1),                                 # left
                    (x - 1, y),                                 # top
                    (x - 1, y - 1)                              # top left
                    ]
        elif x == 0 and y == (self.y_max - 1):  # top right corner
            return [(x, y - 1),                 # left
                    (x + 1, y),                 # bottom
                    (x + 1, y - 1)              # bottom left
                    ]
        elif x == (self.x_max - 1) and y == 0:  # bottom left corner
            return [(x, y + 1),                 # right
                    (x - 1, y),                 # top
                    (x - 1, y + 1)              # top right
                    ]
        elif x == 0:                # top row
            return [
                (x, y - 1),         # left
                (x + 1, y - 1),     # bottom left
                (x + 1, y),         # bottom
                (x + 1, y + 1),     # bottom right
                (x, y + 1)          # right
            ]
        elif y == 0:                # left column
            return [
                (x - 1, y),         # top
                (x - 1, y + 1),     # top right
                (x, y + 1),         # left
                (x + 1, y + 1),     # bottom right
                (x + 1, y)          # bottom
            ]
        elif x == self.x_max - 1:   # bottom row
            return [
                (x, y - 1),         # left
                (x - 1, y - 1),     # top left
                (x - 1, y),         # top
                (x - 1, y + 1),     # top right
                (x, y + 1)          # right
            ]
        elif y == self.y_max - 1:   # right column
            return [
                (x + 1, y),         # bottom
                (x + 1, y - 1),     # bottom left
                (x, y - 1),         # left
                (x - 1, y - 1),     # top left
                (x - 1, y)          # top
            ]
        else:  # internal nodes
            return [
                (x - 1, y - 1),     # top left
                (x, y - 1),         # left
                (x + 1, y - 1),     # bottom left
                (x + 1, y),         # bottom
                (x + 1, y + 1),     # bottom right
                (x, y + 1),         # right
                (x - 1, y + 1),     # top right
                (x - 1, y)          # top
            ]

    def get_unsatisfied_nodes(self, threshold: int):
        """Method that collects the list of the unsatisfied nodes based on its neighbor nodes
        and the set threshold.
        Argument:
            :param threshold: determiner if a node is satisfied or not
        """
        nodes = []
        for n in self.all_nodes:
            if n not in self.empty_nodes:
                neighbor_nodes = self.get_neighbor_nodes(n)
                if not self.is_node_satisfied(n, neighbor_nodes, threshold):
                    nodes.append(n)
        return nodes

    def get_satisfied_nodes(self, threshold):
        """Method that collects the list of the satisfied nodes based on its neighbor nodes
        and the set threshold.
        Argument:
            :param threshold: determiner if a node is satisfied or not
        """
        nodes = []
        for n in self.all_nodes:
            if n not in self.empty_nodes:
                neighbor_nodes = self.get_neighbor_nodes(n)
                if self.is_node_satisfied(n, neighbor_nodes, threshold):
                    nodes.append(n)
        return nodes

    def is_node_satisfied(self, node: tuple, neighbor_nodes: list, threshold: int):
        """Method that evaluates if given node is with the same type or value with
        its neighbor nodes. If threshold is met, it will return True. Otherwise False.
        Argument:
            :param node: index of the node to check
            :param neighbor_nodes: indices of the neighbor nodes
            :param threshold: determiner if inputted node is satisfied or not
        """
        t = 0  # threshold counter
        type_ = self.grid[node[0], node[1]]  # get node type of the input node
        for n in neighbor_nodes:  # get node type of the neighbor nodes
            if type_ == self.grid[n[0], n[1]]:
                t += 1  # if same type increment the threshold counter
        if t >= threshold:
            return True
        else:
            return False

    def satisfy_nodes(self, unsatisfied_nodes: list):
        """This method will randomly pick nodes from the empty nodes and
        swap the values with an unsatisfied node.
        Argument:
            :param unsatisfied_nodes: from the get_unsatisfied_nodes method
        """
        r_node = random.choice(self.empty_nodes)
        for n in unsatisfied_nodes:
            # swap values
            self.grid[r_node[0], r_node[1]], self.grid[n[0], n[1]] = \
                self.grid[n[0], n[1]], self.grid[r_node[0], r_node[1]]

    def execute(self, threshold: int):
        """Entry point of the class which handles the execution of the essential methods.
        Argument:
            :param threshold: determiner if a node is satisfied or not
        """
        # initialize all and empty node list
        self.get_node_list()
        unsatisfied_nodes = self.get_unsatisfied_nodes(threshold)
        self.satisfy_nodes(unsatisfied_nodes)
        # refresh all nodes and empty node list
        self.get_node_list()
        return self.get_unsatisfied_nodes(threshold)
