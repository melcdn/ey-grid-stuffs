import argparse

from .data import GRID
from .schelling import Schelling, GridInvalidNodeTypesCount, GridNoEmptyNodeError
from .log import get_logger

LOG = get_logger(__name__)


def build_parser():
    parser = argparse.ArgumentParser(description="Schelling Model")
    parser.add_argument('-t', '--threshold', required=True,
                        help="Threshold required to satisfy neighboring nodes. e.g. 1, 2 or 3")
    parser.add_argument('-i', '--iteration', default=2000,
                        help="Number of iterations to satisfy the neighboring nodes. default=2000")
    arguments = parser.parse_args()
    return arguments


def handler():
    args = build_parser()
    threshold = int(args.threshold)
    iteration = int(args.iteration)
    main(threshold, iteration, GRID)


def main(threshold, iteration, grid):
    sch = None
    try:
        LOG.info('Script started.')
        LOG.info(f'Threshold\t: {threshold}')
        LOG.info(f'Iteration\t: {iteration}')
        sch = Schelling(grid)  # initialize class
        LOG.info('Input Grid:')
        print(sch.grid)
        sch.validate_node_types()
        unsatisfied = 0
        for i in range(iteration):
            LOG.info(f'Iteration Count: {i + 1}')
            unsatisfied = sch.execute(threshold)
            if len(unsatisfied) == 0:
                LOG.info('There are no more unsatisfied nodes.')
                break
        # end of iteration
        if len(unsatisfied) != 0:
            LOG.warning(f"There are still {len(unsatisfied)} unsatisfied nodes.")
            LOG.warning(f"Increasing the 'iteration' might solve this.")

        LOG.info(f'unsatisfied nodes: {len(unsatisfied)}')
        LOG.info(f'satisfied nodes: {len(sch.get_satisfied_nodes(threshold))}')
        LOG.info(f'Updated grid:')
        print(sch.grid)
        LOG.info('Script completed.')
    except (GridNoEmptyNodeError, GridInvalidNodeTypesCount) as grid_err:
        LOG.error(grid_err)
        LOG.error(f"node types found: {sch.nodes_types}")
    except Exception as ex:
        LOG.exception(ex)


if __name__ == '__main__':
    handler()
