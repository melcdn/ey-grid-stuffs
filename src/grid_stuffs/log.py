import logging
import sys


def get_logger(log_name):
    """Set up a custom logger.
    format: dd-M-yy hh:mm:ss [INFO|DEBUG|WARNING|ERROR|CRITICAL] [module_name] message
    output: 05-Dec-19 16:22:01 [INFO    ] [function_name] info test print
    """
    try:
        logger = logging.getLogger(log_name)
        logger.setLevel(logging.DEBUG)
        log_format = logging.Formatter(
            fmt='%(asctime)s [%(levelname)-8s] [%(funcName)s] %(message)s',
            datefmt='%d-%b-%y %H:%M:%S')
        console_handler = logging.StreamHandler(sys.stdout)  # stderr: red prints; stdout: normal prints
        console_handler.setFormatter(log_format)
        logger.addHandler(console_handler)
        console_handler.setLevel(logging.DEBUG)
        return logger
    except Exception as ex:
        raise Exception(f'error encountered during logger setup: {ex}')
