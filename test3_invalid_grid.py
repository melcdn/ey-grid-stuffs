from src.grid_stuffs.cli import main

"""This is sample if more than 3 node types are found in the grid.
Expected:
    You should see print outs:
    [ERROR   ] [main] There should be only two (3) different node types
    [ERROR   ] [main] node types found: [' ' 'o' 'x' 'y']
"""

GRID_WITH_MORE_THAN_3_UNIQUE_TYPES = [
    ['x', 'x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'o'],
    ['x', 'o', 'o', 'x', 'y', 'x', 'o', 'x', 'o', 'x'],
    ['o', 'x', 'x', ' ', 'x', 'o', 'o', 'x', 'o', 'x'],
    ['o', 'x', 'x', 'x', 'o', 'o', 'x', ' ', 'x', 'o'],
    ['x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'x', 'o'],
    ['x', 'o', 'x', ' ', 'y', 'x', 'o', ' ', 'o', 'x'],
    ['x', 'x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'o'],
    ['x', 'o', ' ', 'y', 'y', 'o', 'o', 'x', 'o', 'o'],
    ['o', 'x', 'x', 'x', 'o', 'o', ' ', 'o', 'x', 'o'],
    ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'x', 'o', 'x'],
]

threshold = 4
iteration = 1000

main(threshold, iteration, GRID_WITH_MORE_THAN_3_UNIQUE_TYPES)
