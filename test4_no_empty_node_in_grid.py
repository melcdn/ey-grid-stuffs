from src.grid_stuffs.cli import main

"""This is sample if there are no empty(' ') nodes in the grid.
Expected:
    You should see print outs:
    [ERROR   ] [main] There should be a blank (' ') node type.
    [ERROR   ] [main] node types found: ['o' 'x']
"""

GRID_WITH_NO_EMPTY_NODE = [
    ['x', 'x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'o'],
    ['x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'o', 'x'],
    ['o', 'x', 'x', 'x', 'x', 'o', 'o', 'x', 'o', 'x'],
    ['o', 'x', 'x', 'x', 'o', 'o', 'x', 'o', 'x', 'o'],
    ['x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'x', 'o'],
    ['x', 'o', 'x', 'o', 'x', 'x', 'o', 'x', 'o', 'x'],
    ['x', 'x', 'o', 'o', 'x', 'o', 'x', 'o', 'x', 'o'],
    ['x', 'o', 'x', 'o', 'x', 'o', 'o', 'x', 'o', 'o'],
    ['o', 'x', 'x', 'x', 'o', 'o', 'o', 'o', 'x', 'o'],
    ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'x', 'o', 'x'],
]

threshold = 4
iteration = 1000

main(threshold, iteration, GRID_WITH_NO_EMPTY_NODE)
