# Schelling Model

This document describes how to test and execute this solution on a **Windows** environment.

## Requirements
These applications should be installed on your computer.
1. [Python 3.x](https://www.python.org/downloads/)
2. [Git Bash](https://git-scm.com/downloads)

## Cloning the Repository
Note: You should have [git bash](https://git-scm.com/downloads) installed before proceeding to below steps:
###### 1. Open your `windows terminal` or `git bash terminal`.
###### 2. Go to your working directory (e.g. Desktop)
```bash
cd %USERPROFILE%\Desktop\
```
###### 3. Clone the repository and go to the package folder.
```
git clone https://gitlab.com/melcdn/ey-grid-stuffs
cd ey-grid-stuffs
```

## Testing the Program
###### Install and Activate Virtual Environment (Recommended)
```
pip install virtualenv
virtualenv .venv
.venv\Scripts\activate
```
###### 1. Installing the package
Inside the package directory (ey-grid-stuffs)
```
pip install .
```
###### 2. Testing the installation
Executing below command should output the **help** document.
```
schelly -h
```
```
usage: schelly [-h] -t THRESHOLD [-i ITERATION]

Schelling Model

optional arguments:
  -h, --help            show this help message and exit
  -t THRESHOLD, --threshold THRESHOLD
                        Threshold required to satisfy neighboring nodes. e.g. 1, 2 or 3
  -i ITERATION, --iteration ITERATION
                        Number of iterations to satisfy the neighboring nodes. default=1000
```
###### 3. Sample command with arguments and result
Executing below command should output something like below.
```
schelly -t 2

07-Nov-20 21:57:16 [INFO    ] [main] Script started.
07-Nov-20 21:57:16 [INFO    ] [main] Threshold  : 2
07-Nov-20 21:57:16 [INFO    ] [main] Iteration  : 1000
07-Nov-20 21:57:16 [INFO    ] [main] Input Grid:
[['x' 'x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'o']
 ['x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'o' 'x']
 ['o' 'x' 'x' ' ' 'x' 'o' 'o' 'x' 'o' 'x']
 ['o' 'x' 'x' 'x' 'o' 'o' 'x' ' ' 'x' 'o']
 ['x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'x' 'o']
 ['x' 'o' 'x' ' ' 'x' 'x' 'o' ' ' 'o' 'x']
 ['x' 'x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'o']
 ['x' 'o' ' ' 'o' 'x' 'o' 'o' 'x' 'o' 'o']
 ['o' 'x' 'x' 'x' 'o' 'o' ' ' 'o' 'x' 'o']
 ['o' 'o' 'o' 'o' 'o' 'o' 'o' 'x' 'o' 'x']]
07-Nov-20 21:57:16 [INFO    ] [main] Iteration Count: 1
07-Nov-20 21:57:16 [INFO    ] [main] Iteration Count: 2
07-Nov-20 21:57:16 [INFO    ] [main] Iteration Count: 3
07-Nov-20 21:57:16 [INFO    ] [main] There are no more unsatisfied nodes.
07-Nov-20 21:57:16 [INFO    ] [main] unsatisfied nodes: 0
07-Nov-20 21:57:16 [INFO    ] [main] satisfied nodes: 94
07-Nov-20 21:57:16 [INFO    ] [main] Updated grid:
[['x' 'x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' ' ']
 ['x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'o' 'x']
 ['o' 'x' 'x' ' ' 'x' 'o' 'o' 'x' 'o' 'x']
 ['o' 'x' 'x' 'x' 'o' 'o' 'x' 'x' 'x' 'o']
 ['x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'x' 'o']
 ['x' 'o' 'x' 'x' 'x' 'x' 'o' ' ' 'o' 'x']
 ['x' 'x' 'o' 'o' 'x' 'o' 'x' 'o' 'x' 'o']
 ['x' 'o' 'x' 'o' 'x' 'o' 'o' 'x' 'o' 'o']
 ['o' 'x' 'x' 'x' 'o' 'o' ' ' 'o' ' ' 'o']
 ['o' 'o' 'o' 'o' 'o' 'o' 'o' 'o' 'o' ' ']]
07-Nov-20 21:57:16 [INFO    ] [main] Script completed.
```
###### 5. Exiting the Virtual Environment (If needed)
```
.venv\Scripts\deactivate.bat
```
## Test Cases
###### 1. Installation of the required package.
```
pip install numpy
```

**Note**: If you used a **virtual environment** in [Testing the Program](#testing-the-program) section, there is no need to install `numpy`.
Just activate the `environment`:
```
.venv\Scripts\activate
```
###### 2. Running the Tests
Refer to the *description (in-line comments)* of each script for more information.
###### a. All Satisfied
```
python test1_all_satisfied.py
```
###### b. Some Satisfied
```
python test2_not_all_satisfied.py
```
###### c. Invalid Grid
```
python test3_invalid_grid.py
```
###### 4. No Empty Nodes
```
python test4_no_empty_node_in_grid.py
```
