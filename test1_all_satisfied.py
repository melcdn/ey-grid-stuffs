from src.grid_stuffs.data import GRID
from src.grid_stuffs.cli import main

"""This is sample if nodes are all satisfied.
Expected:
    You should see print outs:
    [INFO    ] [main] There are no more unsatisfied nodes.
    [INFO    ] [main] unsatisfied nodes: 0
    [INFO    ] [main] satisfied nodes: 94
"""

threshold = 3
iteration = 100

main(threshold, iteration, GRID)

